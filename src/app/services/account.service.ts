import { Injectable } from "@angular/core";
import { ApiService } from "src/app/common/api.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AccountService {
  constructor(private apiService: ApiService) {}

  openLoginPage(params): Observable<any> {
    const urlPath = "https://www.facebook.com/v9.0/dialog/oauth";
    // const urlPath = "https://www.test.com";
    return this.apiService.doGetObservableForQuery(urlPath, params);
  }

  getLogin() {
    return new Observable(observer => {
      observer.next("sample");
    });
  }
}
